class Student
  attr_accessor :first_name , :last_name , :name , :courses
  def initialize(fname , lname)
    @first_name = fname
    @last_name = lname
    @courses = []
    @name = [fname,lname].join(" ")
  end
  
  def enroll(new_course)
    conflicts = []
    @courses.each do |course|
      conflicts << course.conflicts_with?(new_course)
    end
    if conflicts.any?{|conflict| conflict == true}
      raise "conflict"
    else
      @courses << new_course if @courses.include?(new_course) == false
      @courses.last.students << self
    end
  end
  
  def self.courses
    list = []
    courses.each do |course|
      list << course
    end
    list
  end
  
  def course_load
    hash = Hash.new{0}
    @courses.each do |course|
      hash[course.department] += course.credits
    end
    hash
  end
  
  
end
